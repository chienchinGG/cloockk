package com.example.clock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

public class coundount extends AppCompatActivity {
    private int nowValue,minnowValue,hournowValue;
    private NumberPicker mNumberPicker;
    private NumberPicker minNumberPicker;
    private NumberPicker hourNumberPicker;
    private CountDownTimer countdowntimer;

    private Button start;

    private int h,m,s;//用作重設按鈕的紀錄數字

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coundount);

        start=(Button)findViewById(R.id.button);
        start.bringToFront();

        mNumberPicker = findViewById(R.id.numPicker);
        mNumberPicker.setMinValue(0); //設定最小值
        mNumberPicker.setMaxValue(59); //設定最大值
        mNumberPicker.setValue(0); //設定現值
        nowValue = mNumberPicker.getValue(); //取得現值
        //mNumberPicker.setOnValueChangedListener(numPickerOnValueChange); //設定數字變化監聽事件

        minNumberPicker=findViewById(R.id.numPickerM);
        minNumberPicker.setMinValue(0);
        minNumberPicker.setMaxValue(59);
        minNumberPicker.setValue(30);

        hourNumberPicker=findViewById(R.id.numPickerH);
        hourNumberPicker.setMinValue(0);
        hourNumberPicker.setMaxValue(23);
        hourNumberPicker.setValue(0);

        h=0;m=30;s=0;
    }

    public void clockstart(View view){
        hourNumberPicker.setEnabled(false);
        minNumberPicker.setEnabled(false);
        mNumberPicker.setEnabled(false);

        start.setVisibility(View.INVISIBLE);
        if(countdowntimer!=null){
            countdowntimer.cancel();
        }
        nowValue = mNumberPicker.getValue();
        minnowValue=minNumberPicker.getValue();
        hournowValue=hourNumberPicker.getValue();

        if(h!=hournowValue||minnowValue!=m||nowValue!=s){
            h=hournowValue;m=minnowValue;s=nowValue;
        }

        countdowntimer = new CountDownTimer(nowValue*1000+minnowValue*1000*60+hournowValue*1000*60*60,1000) {
            @Override
            public void onTick(long l) {
                mNumberPicker.setValue((int)(l/1000)%60);
                minNumberPicker.setValue((int)(l/60000)%60);
                hourNumberPicker.setValue((int)(l/3600000)%24);
            }

            @Override
            public void onFinish() {
                hourNumberPicker.setEnabled(true);
                minNumberPicker.setEnabled(true);
                mNumberPicker.setEnabled(true);
                mNumberPicker.setValue(0);
            }
        }.start();
    }

    public void clockpause(View view){
        hourNumberPicker.setEnabled(true);
        minNumberPicker.setEnabled(true);
        mNumberPicker.setEnabled(true);

        countdowntimer.cancel();
        start.setVisibility(View.VISIBLE);
    }

    public void clockreset(View view){
        clockpause(view);

        mNumberPicker.setValue(s);
        minNumberPicker.setValue(m);
        hourNumberPicker.setValue(h);
    }

    public void gotoadd(View view) {
        Intent intent = new Intent(this, addclock.class);
        startActivity(intent);
    }
}
